require "./2.rb"

describe "return max suma" do
	it "return 15" do
		expect(licz(5)).to eq(15)
	end
	it "return 0" do
		expect(licz(0)).to eq(0)
	end
	it "return 1" do
		expect(licz(1)).to eq(1)
	end
end
