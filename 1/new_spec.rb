require "./new.rb"

describe "return double value" do
	it "return 10" do
		expect(multiple_by_2(5)).to eq(10)
	end
	it "return 0" do
		expect(multiple_by_2(0)).to eq(0)
	end
end
